package com.lzj.gen.jetbrains;

/**
 * Created by lzj on 2023/11/24
 */
public class FuckJB {

    public static void main(String[] args) throws Exception {
        String baseDir = "D:/jetbra/cert";
        String certFile = baseDir + "/ca.crt";
        String keyFile = baseDir + "/ca.key";
        String powerConfigDir = "D:/jetbra/config-jetbrains";

        //生成证书及power.conf配置文件，只需要生成一次即可，后续不需要再生成
        genCertAndConfig(certFile, keyFile, powerConfigDir);

        //生成全家桶授权证书
        String license = LicenseUtil.genLicenseForAll("jinbaozi", "2099-09-14");
        //生成激码
        String activeCode = CodeUtil.genActiveCode(certFile, keyFile, license);

        System.out.println("activeCode:");
        System.out.println(activeCode);
    }

    /**
     * 生成证书、power.conf配置及激活码
     * @param certFile
     * @param keyFile
     * @param powerConfigBase
     * @param license
     * @return
     * @throws Exception
     */
    private static String genAll(String certFile, String keyFile, String powerConfigBase, String license) throws Exception {
        genCertAndConfig(certFile, keyFile, powerConfigBase);
        return CodeUtil.genActiveCode(certFile, keyFile, license);
    }

    /**
     * 生成证书及power.conf配置
     * @param certFile
     * @param keyFile
     * @param powerConfigBase
     * @throws Exception
     */
    private static void genCertAndConfig(String certFile, String keyFile, String powerConfigBase) throws Exception {
        CertUtil.genCert(certFile, keyFile);
        PowerConfigUtil.genPowerPluginConfig(certFile, powerConfigBase);
    }

}
