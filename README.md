### 描述
此项目用来生成ja-netfilter（https://gitee.com/ja-netfilter/ja-netfilter) power插件的配置及对应的激活码
其原理参见：https://zhuanlan.zhihu.com/p/668667982
### 如何使用
* 从网上自行下载jetbra.zip包
* check out 本工程，打开FuckJB.java，注意设置好证书保存路径,运行,生成power.conf配置及激活码
* 复制生成的power.conf到jetbra/config-jetbrains目录
* 剩下步骤参照jetbra激活步骤

### 免责声明
本项目仅供学习研究使用，请勿用于非法用途，造成一切后果与本人无关